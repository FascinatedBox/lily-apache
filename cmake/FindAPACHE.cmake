include(FindPkgConfig)
include(FindPackageHandleStandardArgs)

pkg_check_modules(APR apr-1)
find_path(
    APACHE_INCLUDE_DIR
    NAMES
        httpd.h
    PATH_SUFFIXES
        httpd
        apache
        apache2
)
find_program(
    APXS_BIN
    NAMES
        apxs
        apxs2
    PATH_SUFFIXES
        apache
        apache2
        httpd
)

if(APXS_BIN)
    exec_program(
        ${APXS_BIN}
        ARGS
            -q
            LIBEXECDIR
        OUTPUT_VARIABLE
            APACHE_MODULE_DIR
    )
endif(APXS_BIN)

find_package_handle_standard_args(
    APACHE

    # Show default message if unable to find Apache.
    DEFAULT_MSG

    # Require these to be set to consider Apache found.
    APACHE_INCLUDE_DIR
    APACHE_MODULE_DIR
    APR_FOUND
)
