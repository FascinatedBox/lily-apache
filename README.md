lily-apache
===========

[View Documentation](https://fascinatedbox.gitlab.io/lily-apache/server/module.server.html)

# Intro

This repository contains a library that provides a bridge that allows the Apache
web server to use Lily to handle requests. Requests are processed in the
interpreter's template mode (`lily -t`).

In template mode, the first file is processed as a template instead of code.
Only text between `<?lily ... ?>` tags is parsed as code, and all content
outside of those tags is treated as content. In mod_lily's case, the content is
sent to Apache to be rendered.

Templates must always start with an code tag, even if the tag is empty. This
requirement allows Lily to skip rendering code-only files since `<?` is not
valid Lily code.

Access to Apache functionality is provided by a `server` module that can be
imported by files in either mode. Additionally, when an import is processed, it
is always done in code mode. Templates are not able to import other templates.

# Setup

During the installation step, make a note of where `mod_lily` is installed.

Open Apache's configuration file and add the following:

```
LoadModule    lily_module    <path-to-mod_lily>

<Directory "/var/www/html/lily">
    SetHandler lily
</Directory>
```

By default, mod_lily does not show traceback. To have traceback, replace the
directory tag above with the following:

```
<Directory "/var/www/html/lily">
    SetHandler lily
    LilyTraceback On
</Directory>
```

You will need to reload Apache for the changes to take place (try
`apachectl restart`).

# Test

Here's a test script to make sure it works.

```
<?lily
    import server
?>
<html>
    <head>
    </head>
    <body>
        <p><?lily server.write_literal("mod_lily works!") ?></p>
    </body>
</html>
```
