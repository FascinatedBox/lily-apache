#include "httpd.h"
#include "ap_config.h"
#include "http_config.h"
#include "http_protocol.h"
#include "util_script.h"

#include "lily.h"
#include "lily_server_bindings.h"

typedef struct {
    lily_hash_val *hash;
    lily_state *s;
} bind_table_data;

void lily_server_HtmlString_new(lily_state *s)
{
    lily_container_val *con = lily_push_super(s, ID_HtmlString(s), 1);

    const char *text = lily_arg_string_raw(s, 0);
    lily_msgbuf *msgbuf = lily_msgbuf_get(s);

    if (lily_mb_html_escape(msgbuf, text) == text)
        lily_con_set(con, 0, lily_arg_value(s, 0));
    else {
        lily_push_string(s, lily_mb_raw(msgbuf));
        lily_con_set_from_stack(s, con, 0);
    }

    lily_return_top(s);
}

void lily_server_Tainted_new(lily_state *s)
{
    lily_container_val *con = lily_push_super(s, ID_Tainted(s), 1);

    lily_con_set(con, 0, lily_arg_value(s, 0));
    lily_return_super(s);
}

void lily_server_Tainted_sanitize(lily_state *s)
{
    lily_container_val *instance_val = lily_arg_container(s, 0);

    lily_call_prepare(s, lily_arg_function(s, 1));
    lily_push_value(s, lily_con_get(instance_val, 0));
    lily_call(s, 1);
    lily_return_value(s, lily_call_result(s));
}

void lily_server__write(lily_state *s)
{
    request_rec *r = (request_rec *)lily_config_get(s)->data;
    lily_container_val *con = lily_arg_container(s, 0);
    const char *to_write = lily_as_string_raw(lily_con_get(con, 0));

    ap_rputs(to_write, r);
    lily_return_unit(s);
}

void lily_server__write_literal(lily_state *s)
{
    request_rec *r = (request_rec *)lily_config_get(s)->data;

    ap_rputs(lily_arg_string_raw(s, 0), r);
    lily_return_unit(s);
}

void lily_server__write_unsafe(lily_state *s)
{
    request_rec *r = (request_rec *)lily_config_get(s)->data;

    ap_rputs(lily_arg_string_raw(s, 0), r);
    lily_return_unit(s);
}

static void add_hash_entry(bind_table_data *table_data, const char *key,
        const char *record)
{
    lily_state *s = table_data->s;

    lily_push_string(s, key);

    lily_container_val *con = lily_push_instance(s, ID_Tainted(s), 1);

    lily_push_string(s, record);
    lily_con_set_from_stack(s, con, 0);
    lily_hash_set_from_stack(s, table_data->hash);
}

static int bind_table_entry(void *data, const char *key, const char *value)
{
    /* Don't allow anything to become a string that has invalid utf-8, because
       Lily's string type assumes valid utf-8. */
    if (lily_is_valid_utf8(key) == 0 ||
        lily_is_valid_utf8(value) == 0)
        return TRUE;

    add_hash_entry((bind_table_data *)data, key, value);
    return TRUE;
}

static void bind_table_as(lily_state *s, apr_table_t *table)
{
    bind_table_data table_data;

    table_data.hash = lily_push_hash(s, 0);
    table_data.s = s;

    apr_table_do(bind_table_entry, &table_data, table, NULL);
}

void lily_server_var_env(lily_state *s)
{
    request_rec *r = (request_rec *)lily_config_get(s)->data;

    ap_add_cgi_vars(r);
    ap_add_common_vars(r);
    bind_table_as(s, r->subprocess_env);
}

void lily_server_var_get(lily_state *s)
{
    request_rec *r = (request_rec *)lily_config_get(s)->data;
    apr_table_t *http_get_args;

    ap_args_to_table(r, &http_get_args);
    bind_table_as(s, http_get_args);
}

void lily_server_var_http_method(lily_state *s)
{
    request_rec *r = (request_rec *)lily_config_get(s)->data;

    lily_push_string(s, r->method);
}

void lily_server_var_post(lily_state *s)
{
    request_rec *r = (request_rec *)lily_config_get(s)->data;
    apr_array_header_t *pairs;
    apr_pool_t *pool;

    apr_pool_create(&pool, r->pool);

    bind_table_data table_data;

    /* Var functions take the top as the result (which will be this). */
    table_data.hash = lily_push_hash(s, 0);
    table_data.s = s;

    int res = ap_parse_form_data(r, NULL, &pairs, -1, 1024 * 8);

    if (res != OK) {
        apr_pool_clear(pool);
        return;
    }

    while (pairs && !apr_is_empty_array(pairs)) {
        ap_form_pair_t *pair = (ap_form_pair_t *) apr_array_pop(pairs);

        if (lily_is_valid_utf8(pair->name) == 0)
            continue;

        apr_off_t len;

        apr_brigade_length(pair->value, 1, &len);

        apr_size_t size = (apr_size_t) len;
        char *buffer = apr_palloc(pool, size + 1);

        if (lily_is_valid_utf8(buffer) == 0)
            continue;

        apr_brigade_flatten(pair->value, buffer, &size);
        buffer[len] = 0;

        add_hash_entry(&table_data, pair->name, buffer);
    }

    apr_pool_clear(pool);
}

void lily_server_var_request_headers(lily_state *s)
{
    request_rec *r = (request_rec *)lily_config_get(s)->data;
    apr_table_t *http_headers = r->headers_in;

    bind_table_as(s, http_headers);
}

LILY_DECLARE_SERVER_CALL_TABLE
